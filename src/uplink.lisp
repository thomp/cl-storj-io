(in-package :storj)

;; 
(defparameter *bucket* nil
  "A string - e.g., \"sj://mybucket\"")

(defparameter uplink-cmd "uplink"
  "A string - e.g., \"/path/to/uplink\"")

(defun uplink (out &rest command)
  "The elements of COMMAND are used, whitespace-separated, to assemble
the command. OUT should be an output stream; however, if OUT is
NIL, output is returned as a string."
  ;; UIOP-LP-OUTPUT: :STREAM means a new stream will be made available
  ;; that can be accessed via PROCESS-INFO-OUTPUT and read from
  (let ((uiop-lp-output :stream))
    (let ((uplink-command (merge-list-of-strings
			   (cons uplink-cmd command)
			   " ")))
      (cond ((not out)
	     (with-output-to-string (s)
	       (uplink-- uplink-command uiop-lp-output s)))
	    (t (uplink-- uplink-command uiop-lp-output out))))))

(defun uplink-- (uplink-command uiop-lp-output-stream out)
  (declare (string uplink-command)
	   (stream out))
  (let ((process-info (uiop:launch-program uplink-command
					   :output uiop-lp-output-stream)))
    (uiop:wait-process process-info)
    ;; read accumulated output
    (let ((stream (uiop:process-info-output process-info)))
      (loop while (listen stream)
	    do (progn (princ (read-line stream) out)
		      (terpri out))))))

(defun uplink-ls (&key bucket (out *standard-output*)
		    (output-format "json"))
  (uplink out
	  "ls"
	  "--output" output-format
	  (or bucket *bucket*)))

(defun uplink-cp-upload (data-spec &optional bucket object-path)
  "Copy data into storj storage. DATA-SPEC is either a path, a
namestring for a path, or a sequence of octets. Return a storj.io
URI."
  (let ((tmp-path nil))
    (when (and (sequencep data-spec)
	       (integerp (elt data-spec 0)))
      (setf tmp-path (uiop/stream::get-temporary-file))
      (octets-to-file data-spec tmp-path))
    (let ((storj-uri (uplink-cp-upload-file (or tmp-path data-spec)
					    bucket
					    object-path)))
      (when tmp-path
	(delete-file tmp-path))
      storj-uri)))

(defun uplink-cp-upload-file (file &optional bucket object-path)
  "Copy data into storj storage. FILE should be a path or a
namestring for a path. Return a storj.io URI."
  (let ((bucket (or bucket *bucket*)))
    (assert (stringp bucket))
    (assert (or (stringp file)
		(pathnamep file)))
    (assert (filep file))
    (let ((uplink-output-as-string
	    (uplink nil
		    "cp"
		    (namestring file)
		    (if object-path
			(concatenate 'string bucket "/" object-path)
			bucket))))
      (with-input-from-string (input-stream uplink-output-as-string)
	(let ((line-1 (read-line input-stream nil nil nil)))
	  (let ((sj-uri (subseq line-1
				(search "sj://" line-1))))
	    sj-uri))))))

;;  "path/within/bucket/to/cakes/cheesecake.jpg"
(defun uplink-cp-download (object-path &key (bucket *bucket*) to)
  "Download the object specified by OBJECT-PATH. If true, TO should be a path or a namestring for a path."
  (download-object-at-sj-uri
   (concatenate 'string bucket "/" object-path)
   :to to))

(defun download-object-at-sj-uri (sj-uri &key to)
  "TO should be a path or a namestring for a path. Return, as a third
value, an indication of whether the file corresponding to the first
value should be treated as a temporary file."
  (let* ((temporaryp (not to))
	 (to (or to
		 (uiop/stream::get-temporary-file))))
    (let ((uplink-output-as-string (with-output-to-string (s)
				     (uplink nil
					     "cp"
					     sj-uri
					     (namestring to)))))
      (values to uplink-output-as-string temporaryp))))

;;use: create a shareable URL
;;uplink share --url sj://my1stbucket/x.png
;;(defun uplink-share . . .

(defun uplink-rm (object-path &optional bucket)
  "Remove an object."
  (let ((bucket (or bucket *bucket*)))
    (uplink *standard-output*
	    "rm"
	    (concatenate 'string bucket object-path))))

(defun uplink-version ()
  "Provide version information."
  (uplink *standard-output* "version" nil))

;;
;; utility functions
;;
(defun filep (x)
  "Indicate whether X seems to specify a file. Note that FILEP does
not test whether file actually exists. This function should only be
used with an understanding of its limitations."
  (let ((pathname (if (pathnamep x)
		      x
		      (parse-namestring x))))
    (and (pathname-name pathname)
	 (not (equal (pathname-directory pathname)
		     '(:absolute))))))
