(in-package :cl-user)

(unless (find-package :cl-storj-io)
  (defpackage :cl-storj-io
    (:nicknames :storj)
    (:use :cl)
    (:export *bucket*
	     uplink-cp-download
	     uplink-cp-upload
	     uplink-ls
	     uplink-rm
	     uplink-version)))
