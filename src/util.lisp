(in-package :storj)

;; DCU string.lisp
(defun merge-list-of-strings (some-list &optional (spacer ""))
  "Given a list of strings SOME-LIST, return a string containing each member of some-list, in the same order as in some-list where each string is separated by SPACER. 

If an object in the list isn't a string, use format nil with ~A to attempt a representation. If SPACER is a string, it will be used as a 'spacer' between arguments [a spacer is also added after the last list member]. (note: LIST-TO-STRING has format ~A functionality -> should we tighten this further consistent with name?)

Note(s): 
- CONCS-LIST is concs which handles list arg
- LIST-TO-STRING doesn't put spacer after last argument and can handle list with any members.
- Timing for this function:

   Evaluation took:
  1.909 seconds of real time
  1.548097 seconds of user run time
  0.068004 seconds of system run time
  13 page faults and
  32,203,680 bytes consed.

  versus recursive version

Evaluation took:
  1.539 seconds of real time
  1.424089 seconds of user run time
  0.084005 seconds of system run time
  1 page fault and
  32,225,520 bytes consed."
  (let ((some-string ""))
    (dolist (str some-list)
      (if (stringp str)
	  (setf some-string (concatenate 'string some-string str spacer))
	  ;; not a string so coerce it
	  (setf some-string (concatenate 'string
					 some-string
					 (format nil "~A" str)
					 spacer))))
    some-string))

;; DFILE file.lisp
(defun octets-to-file (octets pathname &key (if-exists :supersede))
  "Create the file specified by path object PATHNAME (corresponds to first lisp object) such that the file is a text file containing the string SOME-STRING. If the file already exists, it is overwritten ('supersede' on planet lisp). Return value undefined."
  (declare (sequence octets)
	   (pathname pathname))
  (let ((file-stream (open pathname	
			   :direction :output
			   :element-type '(unsigned-byte 8) ; octet
			   :if-exists if-exists
			   :if-does-not-exist :create)))
    (octets-to-stream octets file-stream)
    (close file-stream)))

;; DCU sequence.lisp
(defun sequencep (x)
  ;; Why not just
  ;; (member (type-of x) '(cons vector))  ?
  (dolist (type '(cons vector))
    (if (typep x type)
	(return t))))
