(defsystem cl-storj-io
  :serial t
  :description
  "The CL-STORJ-IO system provides an interface to the uplink command, enabling storage and retrieval of data stored via storj.io."
  :components ((:module "src"
		:components ((:file "packages")
			     (:file "util")
			     (:file "uplink")))))
