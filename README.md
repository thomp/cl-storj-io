# CL-STORJ-IO

An interface to the uplink command. Store and retrieve data stored via storj.io.


## Use

Define the default bucket:

    > (setf *bucket* "sj://mybucket")


Add (upload) a file:

	> (uplink-cp-upload "/path/to/x.pdf")
	"sj://mybucket/x.pdf"



Add (upload) a file, specifying a different storj.io object path:

	> (cl-storj-io::uplink-cp-upload "/home/thomp/tmp/x.pdf" nil "1234--x.pdf")

	"sj://my1stbucket/1234--x.pdf"
	> (cl-storj-io::uplink-ls :output-format "tabbed")
	KIND    CREATED                SIZE      KEY
	OBJ     2022-05-27 06:59:59    57461     1234--x.pdf
	OBJ     2022-05-24 23:49:04    170071    x2.png
	OBJ     2022-05-26 14:01:02    155458    x.png
	OBJ     2022-05-26 14:16:54    57461     x.pdf
	NIL


Get (download) a file:

	> (cl-storj-io::uplink-cp-download "x.pdf")

	#P"/tmp/tmp6LNK20WX"



Listing bucket contents:

    > (uplink-ls :output-format "tabbed")
    KIND    CREATED                SIZE      KEY
    OBJ     2022-05-08 22:02:08    155458    x.png
	STORJ> (uplink-ls)
	{"kind":"OBJ","created":"2022-05-08 22:02:08","size":155458,"key":"x.png"}
	NIL
    > (uplink-ls :out nil)
	"{\"kind\":\"OBJ\",\"created\":\"2022-05-08 22:02:08\",\"size\":155458,\"key\":\"x.png\"}
	"
	>


Removing an object:

	> (uplink-ls)
	KIND    CREATED                SIZE      KEY
	OBJ     2022-05-10 08:51:19    241       .cairo-clockrc
	OBJ     2022-05-08 22:02:08    155458    x.png
	NIL
	> (uplink-rm "/.cairo-clockrc")
	removed sj://mybucket/.cairo-clockrc
	NIL
	> (uplink-ls)
	KIND    CREATED                SIZE      KEY
	OBJ     2022-05-08 22:02:08    155458    x.png
	NIL
	> 


